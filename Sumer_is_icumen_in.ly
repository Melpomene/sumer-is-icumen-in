\version "2.20.0"
\language "deutsch"

\layout {
  indent = #0
  ragged-right = ##f
}

logo = \markup {
  \with-color #(x11-color 'grey80)
  \filled-box #'(0 . 3) #'(-0.6 . 2.4) #0.2
  \hspace #-3.5
  \with-color #(x11-color 'orange)
  \filled-box #'(1 . 2) #'(0.4 . 1.4) #0.1
  \hspace #1.5
}

\header {
  title = "Svmer is icumen in"
  subtitle = "Reading Rota"
  % subsubtitle = "Sommerkanon"
  composer = "13. Jahrhundert"
  % opus = \markup { \italic { Harley 978, } BL }
  tagline = \markup { \typewriter https://gitlab.com/Melpomene/scores }
  copyright = \markup {
    \logo
    Konkordien-Kantorei Mannheim, 2021, 
    %% ( \typewriter { https://creativecommons.org/publicdomain/zero/1.0/deed.de} )
    \sans \bold \circle { CC } \sans \bold \circle { "  0  " }
    %% — Vervielfältigung erwünscht
  }
}

global = {
  \set Score.markFormatter = #format-mark-box-alphabet
  \key f \major
  \time 12/8
}


rota = \relative c'' {
  \clef "treble"
  f4^\markup { 1. } e8 d4 e8 f4 f8 e[( d c)] |
  a4^\markup { 2. } a8 b4 g8 a4. r4. |
  \break
  % \mark \default
  f4^\markup { 3. } a8 g4 b8 a4 a8 g4 f8 |
  a4^\markup { 4. } c8 d4 d8 c4. r4. |
  % \mark \default
  f4. d f r |
  \break
  % \mark \default
  c4 a8 b4 g8 a4 c8 b4 a8 |
  f4 a8 g4 e8 f4. r |
  \break
  % \mark \default
  a4 a8 g4 b8 c4 c8 d4 e8 |
  f4 e8 d4 e8 f4. r |
  % \mark \default
  c4. d c b4( a8) |
  f4 a8 b4 g8 a4. b4 c8 |
  a4 c8 g4 e8 f4. r
}

rotaLyrics = \lyricmode {
  Su -- mer is i -- cu -- men in, __
  lhu -- de sing cuc -- cu!
  Gro -- weþ sed and bl -- oweþ med
  and springþ þe wu -- de nu.
  Sing cuc -- cu!
  A -- we ble -- teþ af -- ter lomb,
  lhouþ af -- ter cal -- ve cu.
  Bul -- luc ster -- teþ, buk -- ke ver -- teþ,
  mu -- rie sing cuc -- cu!
  Cuc -- cu, cuc -- cu, __
  wel sin -- ges þu cu -- cu
  ne swik þu nau -- er nu.
}


pesI = \relative c {
  \clef "bass"
  \time 12/8
  c4.^\markup { 1. }  d c d4( e8) |
  g4.^\markup { 2. }  f g r
}

pesILyrics = \lyricmode {
  Sing cuc -- cu nu __ 
  sing cuc -- cu.
}


\markup{ \bold { Canon à 4 } }
\new Staff {
  <<
    \new Voice = "rota" {
      \global
      \autoBeamOff
      \repeat volta 2 \rota
    }
    \new Lyrics \lyricsto "rota" {
      \rotaLyrics
    }
  >>
}

\markup { \bold { Pes } } 
\new Staff {
  <<
    \new Voice = "pes1" {
      \global
      \repeat volta 2 \pesI
    }
    \new Lyrics \lyricsto "pes1" {
      \pesILyrics
    }
  >>
}
