
!['Svmer is icvmen in', whole folio](https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Sumer_is_icumen_in_-_Summer_Canon_%28Reading_Rota%29_%28mid_13th_C%29%2C_f.11v_-_BL_Harley_MS_978.jpg/800px-Sumer_is_icumen_in_-_Summer_Canon_%28Reading_Rota%29_%28mid_13th_C%29%2C_f.11v_-_BL_Harley_MS_978.jpg)

# Svmer is icvmen in

## Significance in linguistics

> There is a song which most refreshingly indicates that English was
> alive in the fields if not in the courts. It was found in Reading
> Abbey complete with musical notation and is one of the first pieces of
> English that is still comparatively easy to recognise today. Even the
> few words which can seem a bit strange – 'med' (meadow), 'louþ'
> (lows), 'verteþ' (farts) and 'swik' (cease) -- fall into place.
> 
> This is the first verse:
> 
> Sumer is icumen in
> Lhude sing, cuccu.
> Groweþ sed and bloweþ med
> and springþ þe wude nu.
> Sing cuccu.
> Awe bleteþ after lomb
> lhouþ after calve cu.
> Bulloc stertetþ, bucke verteþ,
> murie sing cuccu!
> Cuccu, cuccu,
> wel singes þu cuccu
> ne swikeþ u naver nu.
> 
> The remarkable thing about this song is that there ist not a word of
> French in it.  Words like 'summer', 'come' and 'seed' go directly back
> to the Germanic. 'Spring' and 'wood' can be found in *Beowulf*. 
> 'Loud' and 'sing' are in works authored by Alfred the
> Great. There's a pure line of Old English vocabulary and a taste for
> English song that comes from the land as far from the chivalric songs
> of Bertrand de Born as can be imagined. The French culture of Henry 
> [II or Henry Plantagenet] and Eleanor [of Aquitaine] has not eliminated 
> the common tongue.
>
> -- Melvyn Bragg – “The Adventure of English” (London 2003)


## Fetch audio sample

The *Reading rota* is mentioned in [episode
2](https://www.youtube.com/watch?v=lPtdY2YMrbs) of [The Adventure of
English](https://en.wikipedia.org/wiki/The_Adventure_of_English), a
British televison series from 2003. (The song starts at
[11:15](https://www.youtube.com/watch?v=lPtdY2YMrbs&t=675s) and runs
for 44 seconds.)

The audio part with the song can be extracted with the command
``` shell
  youtube-dl -f 140 https://www.youtube.com/watch?v=lPtdY2YMrbs&t=11m15s
```
(The number 140 in the `-f`-option is the video format code; all possible 
format codes can be listed with the `-F`-option.)
